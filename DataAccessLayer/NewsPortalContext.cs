﻿using DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;

namespace DataAccessLayer
{
    public class NewsPortalContext : DbContext
    {
        public virtual DbSet<CommentModel> Comments { get; set; }
        public virtual DbSet<LookupModel> Lookups { get; set; }
        public virtual DbSet<OrganisationModel> Organisations { get; set; }
        public virtual DbSet<PostExtraModel> PostExtras { get; set; }
        public virtual DbSet<PostModel> Posts { get; set; }
        public virtual DbSet<PostTopicModel> PostTopics { get; set; }
        public virtual DbSet<TagModel> Tags { get; set; }
        public virtual DbSet<TopicModel> Topics { get; set; }
        public virtual DbSet<UserModel> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            optionsBuilder.UseSqlServer(@"Server=127.0.0.1,1433;Database=NewsPortalDb; User Id=SA;Password=Sudo2020!")
                .UseLazyLoadingProxies();
    }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            UserModel superAdmin = new UserModel
            {
                Id = -1,
                UserName = "suadmin",
                FirstName = "Super",
                LastName = "Admin",
                CreatedOn = DateTime.Now
            };

            OrganisationModel newsPortal = new OrganisationModel
            {
                Id = 1,
                Name = "News Portal",
                CreatedById = superAdmin.Id,
                CreatedOn = DateTime.Now
            };

            UserModel newsPortalAdmin = new UserModel
            {
                Id = 1,
                UserName = "bsehic1",
                FirstName = "Benjamin",
                LastName = "Šehić",
                Email = "bsehic1@outlook.com",
                OrganisationId = newsPortal.Id,
                CreatedOn = DateTime.Now
            };

            List<TopicModel> topics = new List<TopicModel>
            {
                new TopicModel
                {
                    Id = 1,
                    Name = "Vijesti",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 2,
                    Name = "BiH",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 3,
                    Name = "Regija",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 4,
                    Name = "Svijet",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 5,
                    Name = "Crna Hronika",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 6,
                    Name = "Biznis",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 7,
                    Name = "Sport",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 8,
                    Name = "Fudbal",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 9,
                    Name = "Košarka",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new TopicModel
                {
                    Id = 10,
                    Name = "Ostalo",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
            };

            List<LookupModel> lookups = new List<LookupModel>
            {
                new LookupModel
                {
                    Id = 1,
                    Category = "PostExtraType",
                    Code = "IMG",
                    Description = "Image",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new LookupModel
                {
                    Id = 2,
                    Category = "PostExtraType",
                    Code = "YTV",
                    Description = "Youtube Video",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new LookupModel
                {
                    Id = 3,
                    Category = "PostExtraType",
                    Code = "HYL",
                    Description = "Hyperlink",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new LookupModel
                {
                    Id = 4,
                    Category = "PostExtraType",
                    Code = "VID",
                    Description = "Video",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
                new LookupModel
                {
                    Id = 5,
                    Category = "PostType",
                    Code = "STP",
                    Description = "Standard Post",
                    CreatedById = superAdmin.Id,
                    CreatedOn = DateTime.Now
                },
            };

            modelBuilder.Entity<CommentModel>()
               .Property(x => x.Id)
               .UseIdentityColumn()
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<CommentModel>()
                .HasOne(x => x.Post)
                .WithMany()
                .HasForeignKey(x => x.PostId);

            modelBuilder.Entity<LookupModel>()
               .Property(x => x.Id)
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<LookupModel>()
                .HasOne(x => x.CreatedBy)
                .WithMany()
                .HasForeignKey(x => x.CreatedById);
            modelBuilder.Entity<LookupModel>()
                 .HasOne(x => x.UpdatedBy)
                 .WithMany()
                 .HasForeignKey(x => x.UpdatedById);
            modelBuilder.Entity<LookupModel>()
                .HasIndex(x => x.Code)
                .IsUnique();
            modelBuilder.Entity<LookupModel>()
                .HasData(lookups);

            modelBuilder.Entity<OrganisationModel>()
               .Property(x => x.Id)
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<OrganisationModel>()
                .HasOne(x => x.CreatedBy)
                .WithMany()
                .HasForeignKey(x => x.CreatedById);
            modelBuilder.Entity<OrganisationModel>()
                 .HasOne(x => x.UpdatedBy)
                 .WithMany()
                 .HasForeignKey(x => x.UpdatedById);
            modelBuilder.Entity<OrganisationModel>()
                .HasData(newsPortal);

            modelBuilder.Entity<PostExtraModel>()
               .Property(x => x.Id)
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<PostExtraModel>()
                .HasOne(x => x.Post)
                .WithMany()
                .HasForeignKey(x => x.PostId);
            modelBuilder.Entity<PostExtraModel>()
                 .HasOne(x => x.Type)
                 .WithMany()
                 .HasForeignKey(x => x.TypeId);
            modelBuilder.Entity<PostExtraModel>()
                .HasIndex(x => new { x.Name, x.PostId }).IsUnique();
            

            modelBuilder.Entity<PostModel>()
               .Property(x => x.Id)
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<PostModel>()
                .HasOne(x => x.Type)
                .WithMany()
                .HasForeignKey(x => x.TypeId);
            modelBuilder.Entity<PostModel>()
                 .HasOne(x => x.Organisation)
                 .WithMany()
                 .HasForeignKey(x => x.OrganisationId);
            modelBuilder.Entity<PostModel>()
                .HasOne(x => x.CreatedBy)
                .WithMany()
                .HasForeignKey(x => x.CreatedById);
            modelBuilder.Entity<PostModel>()
                 .HasOne(x => x.UpdatedBy)
                 .WithMany()
                 .HasForeignKey(x => x.UpdatedById);


            modelBuilder.Entity<PostTopicModel>()
                .HasOne(x => x.Post)
                .WithMany()
                .HasForeignKey(x => x.PostId);
            modelBuilder.Entity<PostTopicModel>()
                .HasOne(x => x.Topic)
                .WithMany()
                .HasForeignKey(x => x.TopicId);

            modelBuilder.Entity<TagModel>()
               .Property(x => x.Id)
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<TagModel>()
                .HasOne(x => x.Post)
                .WithMany()
                .HasForeignKey(x => x.PostId);
            modelBuilder.Entity<TagModel>()
                .HasOne(x => x.CreatedBy)
                .WithMany()
                .HasForeignKey(x => x.CreatedById);
            modelBuilder.Entity<TagModel>()
                 .HasOne(x => x.UpdatedBy)
                 .WithMany()
                 .HasForeignKey(x => x.UpdatedById);

            modelBuilder.Entity<TopicModel>()
               .Property(x => x.Id)
               .ValueGeneratedOnAdd();
            modelBuilder.Entity<TopicModel>()
                .HasOne(x => x.CreatedBy)
                .WithMany()
                .HasForeignKey(x => x.CreatedById);
            modelBuilder.Entity<TopicModel>()
                 .HasOne(x => x.UpdatedBy)
                 .WithMany()
                 .HasForeignKey(x => x.UpdatedById);
            modelBuilder.Entity<TopicModel>()
                .HasData(topics);

            modelBuilder.Entity<UserModel>()
                .Property(x => x.Id)
                .UseIdentityColumn()
                .ValueGeneratedOnAdd();
            modelBuilder.Entity<UserModel>()
                .HasOne(x => x.Organisation)
                .WithMany()
                .HasForeignKey(x => x.OrganisationId);
            modelBuilder.Entity<UserModel>()
                .HasOne(x => x.CreatedBy)
                .WithMany()
                .HasForeignKey(x => x.CreatedById);
            //Da se omoguci da se useri kreiraju pomocu querija, pa se to kasnije moze promijeniti
            modelBuilder.Entity<UserModel>()
                 .HasOne(x => x.UpdatedBy)
                 .WithMany()
                 .HasForeignKey(x => x.UpdatedById);

            modelBuilder.Entity<UserModel>()
                .HasData(superAdmin);
            modelBuilder.Entity<UserModel>()
                .HasData(newsPortalAdmin);
        }

    }
}
