﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DomainModel
{
    public class TagModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public virtual PostModel Post { get; set; }
        public int PostId { get; set; }
        public virtual UserModel CreatedBy { get; set; }
        public int? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public virtual UserModel UpdatedBy { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
