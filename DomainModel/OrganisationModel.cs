﻿using System;

namespace DomainModel
{
    public class OrganisationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual UserModel CreatedBy { get; set; }
        public int? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public virtual UserModel UpdatedBy { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
