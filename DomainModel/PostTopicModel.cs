﻿using System;

namespace DomainModel
{
    public class PostTopicModel
    {
        public int Id { get; set; }
        public virtual PostModel Post { get; set; }
        public int? PostId { get; set; }
        public virtual TopicModel Topic {get;set;}
        public int? TopicId { get; set; }
        public virtual UserModel CreatedBy { get; set; }
        public int? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public virtual UserModel UpdatedBy { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
