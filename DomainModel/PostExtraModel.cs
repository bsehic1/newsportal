﻿using System;

namespace DomainModel
{
    public class PostExtraModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public virtual LookupModel Type { get; set; }
        public int TypeId { get; set; }
        public virtual PostModel Post { get; set; }
        public int? PostId { get; set; }
    }
}
