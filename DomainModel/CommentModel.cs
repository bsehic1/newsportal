﻿using System;

namespace DomainModel
{
    public class CommentModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public string Author { get; set; }
        public virtual PostModel Post { get; set; }
        public int? PostId { get; set; }

        public CommentModel()
        {
            Likes = 0;
            Dislikes = 0;
        }
    }
}
