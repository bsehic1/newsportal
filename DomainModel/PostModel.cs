﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Text;

namespace DomainModel
{
    public class PostModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public virtual LookupModel Type { get;set; }
        public int TypeId { get; set; }
        public int TimesShared { get; set; }
        public int NumberOfComments { get; set; }
        public virtual OrganisationModel Organisation { get; set; }
        public int? OrganisationId { get; set; }
        public virtual UserModel CreatedBy { get; set; }
        public int? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public virtual UserModel UpdatedBy { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public PostModel()
        {
            TimesShared = 0;
            NumberOfComments = 0;
        }
    }
}
