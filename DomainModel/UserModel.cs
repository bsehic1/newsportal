﻿using Microsoft.AspNetCore.Identity;
using System;

namespace DomainModel
{
    public class UserModel : IdentityUser<int>
    {
        public override int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public virtual OrganisationModel Organisation { get; set; }
        public int? OrganisationId { get; set; }
        public virtual UserModel CreatedBy { get; set; }
        public int? CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public virtual UserModel UpdatedBy { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
