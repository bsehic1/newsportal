﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class OrganisationsRepository : IOrganisationsRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public OrganisationsRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(Organisation organisation)
        {
            if(organisation == null)
            {
                throw new ArgumentNullException(nameof(organisation));
            }
            _context.Organisations.Add(_mapper.Map<OrganisationModel>(organisation));
        }

        public async Task Delete(int id)
        {
            var record = await GetByIdAsync(id);
            if (record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.Organisations.Remove(_mapper.Map<OrganisationModel>(record));
        }

        public Organisation GetById(int id)
        {
            return _mapper.Map<Organisation>(_context.Organisations.Find(id));
        }

        public async Task<Organisation> GetByIdAsync(int id)
        {
            var record = await _context.Organisations.FindAsync(id);
            return _mapper.Map<Organisation>(record);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(Organisation organisation)
        {
            if (organisation == null)
            {
                throw new ArgumentNullException(nameof(organisation));
            }
            _context.Organisations.Update(_mapper.Map<OrganisationModel>(organisation));
        }
    }
}
