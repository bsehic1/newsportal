﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Threading.Tasks;

namespace Repository
{
    public class CommentsRepository : ICommentsRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public CommentsRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public void Create(Comment comment)
        {
            if (comment == null)
            {
                throw new ArgumentNullException(nameof(comment));
            }
            _context.Comments.Add(_mapper.Map<CommentModel>(comment));
        }

        public async Task Delete(int id)
        {
            var record = await GetByIdAsync(id);
            if(record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.Comments.Remove(_mapper.Map<CommentModel>(record));
        }

        public Comment GetById(int id)
        {
            return _mapper.Map<Comment>(_context.Comments.Find(id));
        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            var record = await _context.Comments.FindAsync(id);
            return _mapper.Map<Comment>(record);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(Comment comment)
        {
            if (comment == null)
            {
                throw new ArgumentNullException(nameof(comment));
            }
            _context.Comments.Update(_mapper.Map<CommentModel>(comment));
        }
    }
}
