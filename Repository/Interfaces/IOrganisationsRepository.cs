﻿using DataTransferObject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IOrganisationsRepository
    {
        public void Create(Organisation organisation);
        public Organisation GetById(int id);
        public void Update(Organisation organisation);
        public Task Delete(int id);
        public Task<Organisation> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
