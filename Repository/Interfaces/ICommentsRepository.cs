﻿using DataTransferObject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface ICommentsRepository
    {
        public void Create(Comment comment);
        public Comment GetById(int id);
        public void Update(Comment comment);
        public Task Delete(int id);
        public Task<Comment> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
