﻿using DataTransferObject;
using System;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IPostsRepository
    {
        public void Create(Post post);
        public Post GetById(int id);
        public void Update(Post post);
        public Task Delete(int id);
        public Task<Post> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
