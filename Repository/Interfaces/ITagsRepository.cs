﻿using DataTransferObject;
using System;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface ITagsRepository
    {
        public void Create(Tag tag);
        public Tag GetById(int id);
        public void Update(Tag tag);
        public Task Delete(int id);
        public Task<Tag> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
