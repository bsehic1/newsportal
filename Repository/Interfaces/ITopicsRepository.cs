﻿using DataTransferObject;
using System;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface ITopicsRepository
    {
        public void Create(Topic topic);
        public Topic GetById(int id);
        public void Update(Topic topic);
        public Task Delete(int id);
        public Task<Topic> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
