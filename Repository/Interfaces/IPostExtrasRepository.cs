﻿using DataTransferObject;
using System;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IPostExtrasRepository
    {
        public void Create(PostExtra postExtra);
        public PostExtra GetById(int id);
        public void Update(PostExtra postExtra);
        public Task Delete(int id);
        public Task<PostExtra> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
