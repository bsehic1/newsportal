﻿using DataTransferObject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IPostTopicsRepository
    {
        public void Create(PostTopic postTopic);
        public PostTopic GetById(int id);
        public void Update(PostTopic postTopic);
        public Task Delete(int id);
        public Task<PostTopic> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
