﻿using DataTransferObject;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface ILookupsRepository
    {
        public void Create(Lookup lookup);
        public Lookup GetById(int id);
        public void Update(Lookup lookup);
        public Task Delete(int id);
        public Task<Lookup> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
