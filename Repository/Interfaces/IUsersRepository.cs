﻿using DataTransferObject;
using DomainModel;
using System;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
    public interface IUsersRepository
    {
        public void Create(User user);
        public User GetById(int id);
        public void Update(User user);
        public Task Delete(int id);
        public Task<User> GetByIdAsync(int id);
        public Task<bool> SaveChangesAsync();
        public bool SaveChanges();
    }
}
