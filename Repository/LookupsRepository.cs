﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class LookupsRepository : ILookupsRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public LookupsRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(Lookup lookup)
        {
            if (lookup == null)
            {
                throw new ArgumentNullException(nameof(lookup));
            }
            _context.Lookups.Add(_mapper.Map<LookupModel>(lookup));
        }

        public async Task Delete(int id)
        {
            var record = await GetByIdAsync(id);
            if (record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.Lookups.Remove(_mapper.Map<LookupModel>(record));
        }

        public Lookup GetById(int id)
        {
            return _mapper.Map<Lookup>(_context.Lookups.Find(id));
        }

        public async Task<Lookup> GetByIdAsync(int id)
        {
            var record = await _context.Lookups.FindAsync(id);
            return _mapper.Map<Lookup>(record);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(Lookup lookup)
        {
            if (lookup == null)
            {
                throw new ArgumentNullException(nameof(lookup));
            }
            _context.Lookups.Update(_mapper.Map<LookupModel>(lookup));
        }
    }
}
