﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using Utils;

namespace Repository
{
    public class UserRepository : IUsersRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public UserRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            _context.Users.Add(_mapper.Map<UserModel>(user));
        }

        public async Task Delete(int id)
        {
            var record = await GetByIdAsync(id);
            if (record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.Users.Remove(_mapper.Map<UserModel>(record));
        }

        public User GetById(int id)
        {
            return _mapper.Map<User>(_context.Users.Find(id));
        }

        public async Task<User> GetByIdAsync(int id)
        {
            UserModel record = await _context.Users.FindAsync(id);

            return record.toUser();
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            _context.Users.Update(_mapper.Map<UserModel>(user));
        }
    }
}
