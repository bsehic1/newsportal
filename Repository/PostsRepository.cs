﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class PostsRepository:IPostsRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public PostsRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(Post post)
        {
            if (post == null)
            {
                throw new ArgumentNullException(nameof(post));
            }
            _context.Posts.Add(_mapper.Map<PostModel>(post));
        }

        public async Task Delete(int id)
        {
            var record = await GetByIdAsync(id);
            if (record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.Posts.Remove(_mapper.Map<PostModel>(record));
        }

        public Post GetById(int id)
        {
            return _mapper.Map<Post>(_context.Posts.Find(id));
        }

        public async Task<Post> GetByIdAsync(int id)
        {
            var record = await _context.Posts.FindAsync(id);
            return _mapper.Map<Post>(record);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(Post post)
        {
            if (post == null)
            {
                throw new ArgumentNullException(nameof(post));
            }
            _context.Posts.Update(_mapper.Map<PostModel>(post));
        }
    }
}
