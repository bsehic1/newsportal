﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class PostExtrasRepository : IPostExtrasRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public PostExtrasRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(PostExtra postExtra)
        {
            if (postExtra == null)
            {
                throw new ArgumentNullException(nameof(postExtra));
            }
            if (string.IsNullOrEmpty(postExtra.Name))
            {
                throw new ArgumentNullException(nameof(postExtra.Name));
            }
            if (string.IsNullOrEmpty(postExtra.Value))
            {
                throw new ArgumentNullException(nameof(postExtra.Value));
            }
            _context.PostExtras.Add(_mapper.Map<PostExtraModel>(postExtra));
        }

        public async Task Delete(int id)
        {
            var record = await GetByIdAsync(id);
            if (record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.PostExtras.Remove(_mapper.Map<PostExtraModel>(record));
        }

        public PostExtra GetById(int id)
        {
            return _mapper.Map<PostExtra>(_context.PostExtras.Find(id));
        }

        public async Task<PostExtra> GetByIdAsync(int id)
        {
            var record = await _context.PostExtras.FindAsync(id);
            return _mapper.Map<PostExtra>(record);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(PostExtra postExtra)
        {
            if (postExtra == null)
            {
                throw new ArgumentNullException(nameof(postExtra));
            }
            if (string.IsNullOrEmpty(postExtra.Name))
            {
                throw new ArgumentNullException(nameof(postExtra.Name));
            }
            if (string.IsNullOrEmpty(postExtra.Value))
            {
                throw new ArgumentNullException(nameof(postExtra.Value));
            }
            _context.PostExtras.Update(_mapper.Map<PostExtraModel>(postExtra));
        }
    }
}
