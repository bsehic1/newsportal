﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Threading.Tasks;

namespace Repository
{
    public class TagRepository : ITagsRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public TagRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(Tag tag)
        {
            if (tag == null)
            {
                throw new ArgumentNullException(nameof(tag));
            }
            if (string.IsNullOrEmpty(tag.Text))
            {
                throw new ArgumentNullException(nameof(tag.Text));
            }
            _context.Tags.Add(_mapper.Map<TagModel>(tag));
        }

        public async Task Delete(int id)
        {
            var record = await GetByIdAsync(id);
            if (record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.Tags.Remove(_mapper.Map<TagModel>(record));
        }

        public Tag GetById(int id)
        {
            return _mapper.Map<Tag>(_context.Tags.Find(id));
        }

        public async Task<Tag> GetByIdAsync(int id)
        {
            var record = await _context.Tags.FindAsync(id);
            return _mapper.Map<Tag>(record);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(Tag tag)
        {
            if (tag == null)
            {
                throw new ArgumentNullException(nameof(tag));
            }
            if (string.IsNullOrEmpty(tag.Text))
            {
                throw new ArgumentNullException(nameof(tag.Text));
            }
            _context.Tags.Update(_mapper.Map<TagModel>(tag));
        }
    }
}
