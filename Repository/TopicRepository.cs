﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class TopicRepository : ITopicsRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public TopicRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(Topic topic)
        {
            if (topic == null)
            {
                throw new ArgumentNullException(nameof(topic));
            }
            if (string.IsNullOrEmpty(topic.Name))
            {
                throw new ArgumentNullException(nameof(topic.CreatedById));
            }
            _context.Topics.Add(_mapper.Map<TopicModel>(topic));
        }

        public async Task Delete(int id)
        {
            var record = await GetByIdAsync(id);
            if (record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.Topics.Remove(_mapper.Map<TopicModel>(record));
        }

        public Topic GetById(int id)
        {
            return _mapper.Map<Topic>(_context.Topics.Find(id));
        }

        public async Task<Topic> GetByIdAsync(int id)
        {
            var record = await _context.Topics.FindAsync(id);
            return _mapper.Map<Topic>(record);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(Topic topic)
        {
            if (topic == null)
            {
                throw new ArgumentNullException(nameof(topic));
            }
            if (string.IsNullOrEmpty(topic.Name))
            {
                throw new ArgumentNullException(nameof(topic.CreatedById));
            }
            _context.Topics.Update(_mapper.Map<TopicModel>(topic));
        }
    }
}
