﻿using AutoMapper;
using DataAccessLayer;
using DataTransferObject;
using DomainModel;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class PostTopicsRepository : IPostTopicsRepository
    {
        private NewsPortalContext _context;
        private readonly IMapper _mapper;

        public PostTopicsRepository(NewsPortalContext context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public void Create(PostTopic postTopic)
        {
            if (postTopic == null)
            {
                throw new ArgumentNullException(nameof(postTopic));
            }
            _context.PostTopics.Add(_mapper.Map<PostTopicModel>(postTopic));
        }

        public async Task Delete(int id)
        {
            
            var record = await GetByIdAsync(id);
            if (record == null)
            {
                throw new ArgumentOutOfRangeException(nameof(id));
            }
            _context.PostTopics.Remove(_mapper.Map<PostTopicModel>(record));
        }

        public PostTopic GetById(int id)
        {
            return _mapper.Map<PostTopic>(_context.PostTopics.Find(id));
        }

        public async Task<PostTopic> GetByIdAsync(int id)
        {
            var record = await _context.PostTopics.FindAsync(id);
            return _mapper.Map<PostTopic>(record);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(PostTopic postTopic)
        {
            if (postTopic == null)
            {
                throw new ArgumentNullException(nameof(postTopic));
            }
            _context.PostTopics.Update(_mapper.Map<PostTopicModel>(postTopic));
        }
    }
}
