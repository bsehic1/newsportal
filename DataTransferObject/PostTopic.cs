﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObject
{
    public class PostTopic
    {
        public int Id { get; set; }
        public int PostId { get; set; }
        public int TopicId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
