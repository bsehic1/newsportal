﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObject
{
    public class PostExtra
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int TypeId { get; set; }
        public int PostId { get; set; }
    }
}
