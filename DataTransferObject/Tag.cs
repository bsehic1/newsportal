﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObject
{
    public class Tag
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int PostId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
