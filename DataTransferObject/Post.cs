﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataTransferObject
{
    public class Post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public int TypeId { get; set; }
        public int TimesShared { get; set; }
        public int NumberOfComments { get; set; }
        public int OrganisationId { get; set; }
        public int CreatedById { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? UpdatedById { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
