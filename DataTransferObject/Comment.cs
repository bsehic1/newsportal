﻿using System;

namespace DataTransferObject
{
    public class Comment
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public string Author { get; set; }
        public int PostId { get; set; }
    }
}
