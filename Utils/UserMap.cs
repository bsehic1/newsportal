﻿using DataTransferObject;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Utils
{
    public static class UserMap
    {
        public static User toUser(this UserModel userModel)
        {
            return new User
            {
                Id = userModel.Id,
                OrganisationId = userModel.OrganisationId,
                FirstName = userModel.FirstName,
                MiddleName = userModel.MiddleName,
                LastName = userModel.LastName,
                PhoneNumber = userModel.PhoneNumber,
                Email = userModel.Email,
                UserName = userModel.UserName,
                CreatedById = userModel.CreatedById,
                CreatedOn = userModel.CreatedOn,
                UpdatedById = userModel.UpdatedById,
                UpdatedOn = userModel.UpdatedOn
            };
        }

        public static User toUserModel(this User user)
        {
            return new User
            {
                Id = user.Id,
                OrganisationId = user.OrganisationId,
                FirstName = user.FirstName,
                MiddleName = user.MiddleName,
                LastName = user.LastName,
                PhoneNumber = user.PhoneNumber,
                Email = user.Email,
                UserName = user.UserName,
                CreatedById = user.CreatedById,
                CreatedOn = user.CreatedOn,
                UpdatedById = user.UpdatedById,
                UpdatedOn = user.UpdatedOn
            };
        }
    }
}
