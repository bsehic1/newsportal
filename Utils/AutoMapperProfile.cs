﻿using AutoMapper;
using DataTransferObject;
using DomainModel;
using System;
using System.Security.Cryptography.X509Certificates;

namespace Utils
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Comment, CommentModel>().ReverseMap();
            CreateMap<Lookup, LookupModel>().ReverseMap();
            CreateMap<Organisation, OrganisationModel>().ReverseMap();
            CreateMap<PostExtra, PostExtraModel>().ReverseMap();
            CreateMap<Post, PostModel>().ReverseMap();
            CreateMap<PostTopic, PostTopicModel>().ReverseMap();
            CreateMap<Tag, TagModel>().ReverseMap();
            CreateMap<Topic, TopicModel>().ReverseMap();
            CreateMap<User, UserModel>().ReverseMap();
        }
    }
}
