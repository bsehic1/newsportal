﻿using System;
using System.Net.Mime;
using System.Threading.Tasks;
using DataTransferObject;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;

namespace Controller.Controllers
{
    [Route("api/v1/organisations")]
    [ApiController]
    public class OrganisationsController : ControllerBase
    {
        private IOrganisationsRepository _organisationRepository;
        private readonly ILogger _logger;

        public OrganisationsController(IOrganisationsRepository organisationRepository, ILogger<OrganisationsController> logger)
        {
            _organisationRepository = organisationRepository ?? throw new ArgumentNullException(nameof(organisationRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost(Name = "Create Organisation")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Organisation>> CreateAsync(Organisation organisation)
        {
            try
            {
                _organisationRepository.Create(organisation);
                await _organisationRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return CreatedAtAction(nameof(GetByIdAsync), new { id = organisation.Id }, organisation);
        }

        [HttpGet("{id}", Name = "Get Organisation By Id")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Organisation>> GetByIdAsync(int id)
        {
            try
            {
                var organisation = await _organisationRepository.GetByIdAsync(id);
                return Ok(organisation);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }

        }

        [HttpPut(Name = "Update Organisation")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> UpdateAsync(Organisation organisation)
        {
            try
            {
                _organisationRepository.Update(organisation);
                await _organisationRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return Ok("Organisation updated succesfully!");
        }

        [HttpDelete(Name = "Delete Organisation")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await _organisationRepository.Delete(id);
                await _organisationRepository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return Ok("Organisation deleted successfully!");
        }
    }
}
