﻿using System;
using System.Net.Mime;
using System.Threading.Tasks;
using DataTransferObject;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;

namespace Controller.Controllers
{
    [Route("api/v1/comments")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private ICommentsRepository _commentRepository;
        private readonly ILogger _logger;

        public CommentsController(ICommentsRepository commentRepository, ILogger<CommentsController> logger)
        {
            _commentRepository = commentRepository ?? throw new ArgumentNullException(nameof(commentRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost(Name = "Create Comment")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Comment>> CreateAsync(Comment comment)
        {   
            try
            {
                _commentRepository.Create(comment);
                await _commentRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return CreatedAtAction(nameof(GetByIdAsync), new { id = comment.Id }, comment);
        }

        [HttpGet("{id}", Name = "Get Commnet By Id")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Comment>> GetByIdAsync(int id)
        {
            try
            {
                var comment =  await _commentRepository.GetByIdAsync(id);
                if(comment == null)
                {
                    return BadRequest("No Commnet with that ID!");
                }
                return Ok(comment);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }

        }

        [HttpPut(Name = "Update Comment")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> UpdateAsync(Comment comment)
        {
            try
            {
                _commentRepository.Update(comment);
                await _commentRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return Ok("Comment updated succesfully!");
        }

        [HttpDelete(Name = "Delete Comment")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await _commentRepository.Delete(id);
                await _commentRepository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return Ok("Comment deleted successfully!");
        }

    }
}
