﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using DataTransferObject;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;

namespace Controller.Controllers
{
    [Route("api/v1/topics")]
    [ApiController]
    public class TopicsController : ControllerBase
    {
        private ITopicsRepository _topicRepository;
        private readonly ILogger _logger;

        public TopicsController(ITopicsRepository topicRepository, ILogger<TopicsController> logger)
        {
            _topicRepository = topicRepository ?? throw new ArgumentNullException(nameof(topicRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost(Name = "Create Topic")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Topic>> CreateAsync(Topic topic)
        {
            try
            {
                _topicRepository.Create(topic);
                await _topicRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return CreatedAtAction(nameof(GetByIdAsync), new { id = topic.Id }, topic);
        }

        [HttpGet("{id}", Name = "Get Topic By Id")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Topic>> GetByIdAsync(int id)
        {
            try
            {
                var topic = await _topicRepository.GetByIdAsync(id);
                if (topic == null)
                {
                    return BadRequest("No Topic with that ID!");
                }
                return Ok(topic);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }

        }

        [HttpPut(Name = "Update Topic")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> UpdateAsync(Topic topic)
        {
            try
            {
                _topicRepository.Update(topic);
                await _topicRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return Ok("Topic updated succesfully!");
        }

        [HttpDelete(Name = "Delete Topic")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await _topicRepository.Delete(id);
                await _topicRepository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return Ok("Topic deleted successfully!");
        }
    }
}
