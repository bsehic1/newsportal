﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using DataTransferObject;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Repository.Interfaces;

namespace Controller.Controllers
{
    [Route("api/v1/lookups")]
    [ApiController]
    public class LookupsController : ControllerBase
    {
        private ILookupsRepository _lookupRepository;
        private readonly ILogger _logger;

        public LookupsController(ILookupsRepository lookupRepository, ILogger<LookupsController> logger)
        {
            _lookupRepository = lookupRepository ?? throw new ArgumentNullException(nameof(lookupRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpPost(Name = "Create Lookup")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Lookup>> CreateAsync(Lookup lookup)
        {
            try
            {
                _lookupRepository.Create(lookup);
                await _lookupRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return CreatedAtAction(nameof(GetByIdAsync), new { id = lookup.Id }, lookup);
        }

        [HttpGet("{id}", Name = "Get Lookup By Id")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Lookup>> GetByIdAsync(int id)
        {
            try
            {
                var lookup = await _lookupRepository.GetByIdAsync(id);
                if (lookup == null)
                {
                    return BadRequest("No Lookup with that ID!");
                }
                return Ok(lookup);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }

        }

        [HttpPut(Name = "Update Lookup")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> UpdateAsync(Lookup lookup)
        {
            try
            {
                _lookupRepository.Update(lookup);
                await _lookupRepository.SaveChangesAsync();

            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return Ok("Lookup updated succesfully!");
        }

        [HttpDelete(Name = "Delete Lookup")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> DeleteAsync(int id)
        {
            try
            {
                await _lookupRepository.Delete(id);
                await _lookupRepository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
            return Ok("Lookup deleted successfully!");
        }

    }
}
